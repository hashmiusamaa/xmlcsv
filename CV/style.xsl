<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
      <head>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

          <!-- Optional theme -->
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous" />

          <title>CV of <xsl:apply-templates select="/CurriculaVitae/CurriculumVitae/PersonName" /></title>
      </head>
      <style>
          *{
            margin: 0px;
            padding: 0px;
          }

          .WebPage{
            margin-top: -10px;
            padding-top: -10px;
          }

          h1{
            margin-bottom: -5px;
          }

          h2,h3,h4,h5,h6{
            margin: 10px;
          }
          .Objective{
            margin-top:5px;
            border-top: 2px solid black;
            border-bottom: 2px solid black;
            padding-bottom: 0px;
          }
          p{
            margin: -3px;
            padding: -3px;
            padding-bottom: 0px;
            margin-bottom: 0px;
            margin-left: 5px;
            margin-right: 5px;
          }
          .LeftColumn{
            background: #AAA;
          }
          .Tab{
            padding: 4px;
            background: #333;
            color: #DDD;
            cursor: pointer;
          }
          .Tab:hover{
            background: #FFF;
            color: #333;
          }

      </style>

      <body style="width:100%;">
          <center><h1 style="margin-bottom:20px;">CV Collection</h1></center>
          <div style="max-width:1080px; margin: 0px auto;">
              <xsl:for-each select="CurriculaVitae/CurriculumVitae">
                  <span class="Tab" id="tab{@ID}">
                      <xsl:value-of select="PersonName" />
                  </span>
              </xsl:for-each>
          </div>

          <xsl:apply-templates select="CurriculaVitae" />

          <!-- Latest compiled and minified JavaScript -->
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous" />
          <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"/>
            <script>
                $(document).ready(function(){
                    $('.FullCV').hide();
                    $('.Tab').click(function(){
                        $('.FullCV').hide();
                       $('#'+$(this).attr('id').substring(3,$(this).attr('id').length)).show();
                        document.title = "CV of " +$(this).text();
                    });
                });
            </script>
      </body>
  </html>
</xsl:template>


    <xsl:template match="CurriculaVitae">
        <xsl:apply-templates select="CurriculumVitae" />
    </xsl:template>

    <xsl:template match="CurriculumVitae" >
        <div class="FullCV" id="{@ID}" style="max-width:1080px; margin: 0px auto; background: #CCC">

            <header style="background: #333; height:80px;">
                <center>
                    <xsl:apply-templates select="PersonName" />
                    <xsl:for-each select="ContactInformation/*[@link]">
                        <a href="{@link}"><xsl:value-of select="name(.)"/> | </a>
                    </xsl:for-each>
                </center>
            </header>
            <xsl:apply-templates select="Objective" />

            <div class="container-fluid">
                <div class="LeftColumn col-xs-4" style="text-align: right; padding: 5px">
                    <xsl:apply-templates select="ContactInformation" />
                    <xsl:apply-templates select="Languages" />
                    <xsl:apply-templates select="Skills" />
                    <xsl:apply-templates select="Interests" />
                </div>
                <div class="RightColumn col-xs-8">
                    <xsl:apply-templates select="EducationalInstitutesAttended" />
                    <xsl:apply-templates select="WorkExperience" />
                    <xsl:apply-templates select="Projects" />
                    <xsl:apply-templates select="AwardsAndAchievements" />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="PersonName">
        <h1 style="margin-bottom: -5px; padding-top:15px;">
            <span style="color:#AAA"><xsl:value-of select="FirstName" /></span>
            <span style="color:#DDD"><xsl:value-of select="LastName" /></span>
        </h1><a>| </a>
    </xsl:template>

    <xsl:template match="Objective">
        <div class="Objective">
            <h4>
                Objective
            </h4>
            <p>
                <xsl:value-of select="." />
            </p>
        </div>
    </xsl:template>

    <xsl:template match="ContactInformation">
        <h4>Contact</h4>
        <xsl:apply-templates select="Address" />
        <p>
            <xsl:value-of select="PhoneNumber" />
        </p>
        <p>
            <xsl:value-of select="Email/@primaryEmail" />
        </p>
    </xsl:template>

    <xsl:template match="Address">
        <p>
            <xsl:value-of select="Street" />
        </p>
        <p>
            <xsl:value-of select="City" />,
            <xsl:value-of select="State" />,
            <xsl:value-of select="Country" />
        </p>
    </xsl:template>

    <xsl:template match="Languages" >
        <h4>Languages</h4>
        <xsl:for-each select="Item">
            <span><xsl:value-of select="." /> </span>
            <span> (<xsl:value-of select="@level" />/5)  </span>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="Skills">
        <xsl:apply-templates select="ProgrammingLanguages" />
        <xsl:apply-templates select="Databases" />
        <xsl:apply-templates select="OperatingSystems" />
        <xsl:apply-templates select="Frameworks" />
        <xsl:apply-templates select="Softwares" />
    </xsl:template>

    <xsl:template match="ProgrammingLanguages" >
        <h4>Programming Languages</h4>
            <xsl:apply-templates select="Item" />
    </xsl:template>

    <xsl:template match="Databases" >
        <h4>Databases</h4>
        <xsl:apply-templates select="Item" />
    </xsl:template>

    <xsl:template match="OperatingSystems" >
        <h4>Operating Systems</h4>
        <xsl:apply-templates select="Item" />
    </xsl:template>

    <xsl:template match="Frameworks" >
        <h4>Frameworks</h4>
        <xsl:apply-templates select="Item" />
    </xsl:template>

    <xsl:template match="Softwares" >
        <h4>Softwares</h4>
        <xsl:apply-templates select="Item" />
    </xsl:template>

    <xsl:template match="Item">
        <span><xsl:value-of select="." /> </span>
        <span> (<xsl:value-of select="@level" />/5)  </span>
    </xsl:template>

    <xsl:template match="Interests">
        <xsl:apply-templates select="Professional" />
        <xsl:apply-templates select="Personal" />
    </xsl:template>
    <xsl:template match="Professional">
        <h4>Professional</h4>
        | <xsl:apply-templates select="Interest" />
    </xsl:template>

    <xsl:template match="Personal">
        <h4>Personal</h4>
        | <xsl:apply-templates select="Interest" />
    </xsl:template>

    <xsl:template match="Interest">
        <span><xsl:value-of select="." /> | </span>
    </xsl:template>

    <xsl:template match="EducationalInstitutesAttended" >
        <h3>Education</h3>
        <table>
        <xsl:apply-templates select="Institute" />
        </table>
    </xsl:template>

    <xsl:template match="Institute">
        <style>
            table{
                background: #DDD;
                width:100%;
                border-bottom: 2px solid black;
            }
            table tr{
                padding: 5px;
                border-bottom: 1px solid black;
            }
            table tr td{
                padding: 5px;
            }

        </style>
            <tr>
                <td>
                    <p><b><xsl:value-of select="InstituteName" /></b>
                    <i style="font-size: 12px;"><xsl:apply-templates select="TimePeriod" /></i></p>
                </td>
                <td>
                    <b><xsl:value-of select="Degree" /></b>
                    <p style="font-size: 13px;"><i><b>Specialization:</b></i>
                        <xsl:apply-templates select="Specializations" /></p>
                    <p style="font-size: 13px;"><i><b>Courses:</b></i>
                    <xsl:apply-templates select="MajorOrRelatedCourses" /></p>
                </td>
            </tr>
    </xsl:template>

    <xsl:template match="TimePeriod">
        <p><xsl:value-of select="Begin" /> - <xsl:value-of select="End" /></p>
    </xsl:template>

    <xsl:template match="Department/TimePeriod">
        <p><xsl:value-of select="Begin" /> - <xsl:value-of select="End" /></p>
    </xsl:template>

    <xsl:template match="Specializations">
        <xsl:apply-templates select="Specialization" /> |
    </xsl:template>

    <xsl:template match="Specialization">
        | <xsl:value-of select="(.)" />
    </xsl:template>

    <xsl:template match="MajorOrRelatedCourses">
        <xsl:apply-templates select="Course" /> |
    </xsl:template>

    <xsl:template match="Course">
        | <xsl:value-of select="(.)" />
    </xsl:template>

    <xsl:template match="WorkExperience">
        <h3>Work Experience</h3>
        <table style="width:100%">
            <xsl:apply-templates select="Company" />
        </table>
    </xsl:template>

    <xsl:template match="Company">
        <tr>
            <td>
                <b><xsl:value-of select="@name" /></b><i style="float:right"><xsl:value-of select="Department/TimePeriod" /></i><br/>
                <xsl:apply-templates select="Department" />
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="Department">
        <xsl:value-of select="Designation" /> - <xsl:value-of select="@name" />
        <xsl:apply-templates select="Responsibilities" />
    </xsl:template>

    <xsl:template match="Responsibilities">
        <xsl:apply-templates select="DescItem" />
    </xsl:template>

    <xsl:template match="DescItem">
        <p><span class="glyphicon glyphicon-ok" style="font-size: 9px; margin-right: 10px;" /> <xsl:value-of select="." /></p>
    </xsl:template>


    <xsl:template match="Projects">
        <h3>Projects</h3>
        <table style="width:100%">
            <xsl:apply-templates select="Project" />
        </table>
    </xsl:template>

    <xsl:template match="Project">
        <tr><td>
            <b><xsl:value-of select="@title" /></b><br/>
            <span class="glyphicon glyphicon-ok" style="font-size: 9px; margin-right: 10px;" /> <xsl:value-of select="." /><br/>
        </td></tr>
    </xsl:template>

    <xsl:template match="AwardsAndAchievements">
        <h3>Awards and Achievements</h3>
        <table>
            <xsl:apply-templates select="Award" />
            <xsl:apply-templates select="Achievement" />
        </table>
    </xsl:template>

    <xsl:template match="Award">
            <p><span class="glyphicon glyphicon-ok" style="font-size: 9px; margin-right: 10px;" /><xsl:value-of select="." /></p>
    </xsl:template>
<xsl:template match="Achievement">
        <p><span class="glyphicon glyphicon-ok" style="font-size: 9px; margin-right: 10px;" /><xsl:value-of select="." /></p>
</xsl:template>
</xsl:stylesheet>
